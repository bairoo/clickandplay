PLAYGROUND TASK

This project contains four main test cases with Gherkin Langiuage and and their step definitions files with JAVA. 
The JAVA codes (the scritp) can be tested with different data sets with Cucumber BDD framework. 
I used a five differnt tag as an annotation name in Lithuanian Language to determine which test case can be executed with the runner class. 
To perform any automation task, open the game.future file and select those tag names like "@laimute" or "@bubilas" @laimute Feature:

E.G :

@malunas Scenario: Player properly complete the grid

Given open the url on Chrome web browser
When click the all outer cells on the grid, do not click the inner cells
And the pop-box should open with a display message
Then enter a valid number in boundary to start again with a new size of grid
and copy this tag and go the Runner file and in CukesRunner class line 14 paste at tag place then run it.

Test case id: TC001

Unit to test: Verify if the all cells are clickable?

Assumptions: Windows user playing with the standart USB mause Test data: Go to given URL on Chrome Browser Steps to be executed: 
1. After the user clicks all icons in the outer perimeter, the dialog should appear. 
The result might look like this depending on the size of the grid: None of the cells on the inside should be clicked like in this example. 
2. In the dialog the user should be able to define size of the grid. 
3. After the user defines the size of the grid, the grid should have appropriate number of columns and rows.

Expected Result: When the user clicks all icons in the outer perimeter, the dialog should appear Actual result: ? Pass/Fail Comments

Basic Format of Test Case Statement Verify: Verifiyng the functionality of the cells in different cases according to the requirements Using: Windows OS, Chrome Web driver, tag name With: Mause or keyboard To: [what is returned, shown, demonstrated] Pop-up box converts the pattern (num1+num2) format as a string instead of a number. ie. (9+1) was entered JS injection was accepted but it shoul not be like that Verify: Used as the first word of the test statement. Using: To identify what is being tested. You can use ‘entering’ or ‘selecting’ here instead of using depending on the situation. The player should b able to complette the external perimeter. NOTE: For any application, you need to cover all types of tests as: Functional cases Negative cases Boundary value cases While writing these all your TC’s should be simple and easy to understand